/*
 * Autor:				Achim H�ckler
 * Gruppe:				2913
 * Erstellungsdatum: 	11.2.2016
 */

public class messen {
	private String mylink="";
	private String mytag="";
	
	messen(String link, String tag){
		this.mylink=link;
		this.mytag=tag;
	}
	//Erstellte ein Objekt zur DOM/SAX suche und gibt dann die Suchdauer zur�ck
	public long zeitmessdom(){
		long timestart = System.currentTimeMillis();//zeit vor beginn
		myDOM Test = new myDOM(this.mylink, this.mytag);
		Test.suche();	
		return (Test.gethittime() - timestart);
	}
	
	public long zeitmesssax(){
		long timestart = System.currentTimeMillis();
		mySAX saxtest = new mySAX();
		saxtest.parse(this.mylink, this.mytag);
		return (saxtest.gethit() - timestart);
	}

}
