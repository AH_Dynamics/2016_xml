/*
 * Autor:				Achim Häckler
 * Gruppe:				2913
 * Erstellungsdatum: 	11.2.2016
 */

import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class myDOM {
	private String filelink;
	private String search;
	public DocumentBuilder builder;
	public Document document;
	private long hittime = 0;

	public myDOM(String link, String input) {
		this.filelink = link;
		this.search = input;
	}

	public long gethittime() {
		return this.hittime;
	}

	public boolean suche() {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {

			System.err.println("Fehler bei Parsererstellung");

		}
		try {
			document = builder.parse(ClassLoader.getSystemResourceAsStream(this.filelink));

		} catch (Exception e) {
			System.err.println("Fehler beim Laden der Datei");
		}
		
		//Nodelist mit allen Childnodes Laden -> ganzer Dombaum
		NodeList nodeList = document.getDocumentElement().getChildNodes();
		
		//Alle Teile der Nodelist durchgehn
		for (int i = 0; i < nodeList.getLength(); i++) {
			//alle Teile des Elements sollen augegeben werden ....
			boolean outputswitcher = false;
			//Node aus Nodelist auswählen
			Node node = nodeList.item(i);
			
			if (node instanceof Element) {
				//ChildNodes durchgehen ...
				NodeList childNodes = node.getChildNodes();
				for (int v = 0; v < childNodes.getLength(); v++) {
					Node cNode = childNodes.item(v);
					if (cNode instanceof Element) {
						String inhalt = cNode.getLastChild().getTextContent().trim();
						//Inhalt des Nodes auf Übereinstimmung mit Suchparameter testen
						if (inhalt.equalsIgnoreCase(search)) {
							// System.out.println(inhalt);
							outputswitcher = true;
						}
						//Bei Treffer ausgeben
						if (outputswitcher == true)
							System.out.println(inhalt);						
						}
					//Bei letztem Teil des Treffers Absatz ausgeben zeit messen und Suche beenden 
					if ((outputswitcher == true) && (v == childNodes.getLength()-1)) {
						System.out.println();
						this.hittime = System.currentTimeMillis();
						return true;
					}
				}
			}
		}

		return false;
	}

}
