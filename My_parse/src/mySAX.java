/*
 * Autor:				Achim H�ckler
 * Gruppe:				2913
 * Erstellungsdatum: 	11.2.2016
 */

import java.io.IOException;

import javax.xml.parsers.*;

import org.xml.sax.SAXException;

public class mySAX {
	private long hit = 0;

	public void parse(String link, String search) {
		SAXParserFactory parserFactor = SAXParserFactory.newInstance();
		SAXParser parser = null;
		try {
			parser = parserFactor.newSAXParser();
		} catch (ParserConfigurationException e) {
			System.err.println("Fehler bei Parserkonfiguration");
			e.printStackTrace();
		} catch (SAXException e) {
			System.err.println("Saxfehler");
			e.printStackTrace();
		}
		mySaxHandler handler = new mySaxHandler(search);
		try {
			parser.parse(ClassLoader.getSystemResourceAsStream(link), handler);
		} catch (SAXException e) {
			System.err.println("Saxfehler");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Fehler beim Laden der Datei");
			e.printStackTrace();
		}
		this.hit = handler.gethittime();

	}

	public long gethit() {
		return this.hit;
	}

}
