/*
 * Autor:				Achim H�ckler
 * Gruppe:				2913
 * Erstellungsdatum: 	11.2.2016
 */

import org.xml.sax.helpers.*;

public class mySaxHandler extends DefaultHandler {
	private String content="";
	private String suchen="";
	private long hittime=0;
	private boolean sysoall=false;
	mySaxHandler(String search){//Ziel �bergeben
		this.suchen=search;
	}
	public void endElement(String namespaceURI,String localName,String qname){
		//wenn enthaltener String suche entspricht
		if(this.content.equalsIgnoreCase(this.suchen)){
			this.sysoall=true;			
		}
		if(this.sysoall==true){
			System.out.println(content);
		}
		
		if(this.sysoall==true && qname.equalsIgnoreCase("preis")){
			System.out.println();
			this.hittime=System.currentTimeMillis();
			this.sysoall=false;
		}
	}
	public void characters(char []ch,int start,int length){		
		this.content=new String(ch, start, length);
	}
	public long gethittime(){
		return this.hittime;
	}
}
