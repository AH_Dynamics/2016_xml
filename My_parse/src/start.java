/*
 * Autor:				Achim H�ckler
 * Gruppe:				2913
 * Erstellungsdatum: 	11.2.2016
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/*
 * Autor:				Achim H�ckler
 * Gruppe:				2913
 * Erstellungsdatum: 	11.2.2016
 */

public class start {

	public static void main(String[] args) {
		// ArrayList zum speichern der gesuchten Elemente
		ArrayList<String> suchtags = new ArrayList<String>();
		// ArrayLists zum speichern der Messergebnisse
		ArrayList<Long> domzeit = new ArrayList<Long>();
		ArrayList<Long> saxzeit = new ArrayList<Long>();
		//Suchparameter
		suchtags.add("Yggdrasil");
		suchtags.add("Rosie");
		suchtags.add("Schlinge");
		suchtags.add("Mangrove");
		suchtags.add("Schwinger");
		suchtags.add("Dornie");
		suchtags.add("Palme");
		suchtags.add("Kirsche");
		suchtags.add("Tarzans");
		suchtags.add("Buschie");		
		for (int i = 0; i < suchtags.size(); i++) {
			
			//neues Messobjekt erstellen
			messen Test = new messen("bigwood.xml", suchtags.get(i));
			//DOMmessung durchf�hren, dann SAXmessung durchf�hren	
			domzeit.add(Test.zeitmessdom());			
			saxzeit.add(Test.zeitmesssax());
			
		}
		BufferedWriter schreiben=null;
		try {
			schreiben = new BufferedWriter(new FileWriter("ergebnis.txt"));

		} catch (IOException e1) {

			System.err.println("Konnte Datei nicht schreiben!");
		}
		
		//Gemessene Zeiten in Datei ausgeben
		String writefile="";
		writefile+="Tag			DOM			SAX\n";
		for (int u=0;u<suchtags.size();u++){
			writefile+=suchtags.get(u)+"		" + domzeit.get(u)+ "		"+saxzeit.get(u)+"\n";
			try {
				schreiben.write(writefile);
				schreiben.flush();
				writefile = "";
			} catch (IOException e) {
				System.err.println("Konnte Datei nicht schreiben!");
			}
		}
		try {
			schreiben.close();
		} catch (IOException e) {
			System.err.println("Fehler BufferedWriter - close()");
			e.printStackTrace();
		}
		

	}

}
