import java.util.*;

import javax.xml.parsers.*;

import org.w3c.dom.*;

public class DOM {
	public static void main(String[] args) throws Exception {
		long start = System.currentTimeMillis();
		// Get the DOM Builder Factory
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		// Get the DOM Builder
		DocumentBuilder builder = factory.newDocumentBuilder();

		// Load and Parse the XML document
		// document contains the complete XML as a Tree.
		Document document = builder.parse(ClassLoader.getSystemResourceAsStream("xml/daten.xml"));

		List<Gehoelz> empList = new ArrayList<>();

		// Iterating through the nodes and extracting the data.
		NodeList nodeList = document.getDocumentElement().getChildNodes();

		for (int i = 0; i < nodeList.getLength(); i++) {

			// We have encountered an <employee> tag.
			Node node = nodeList.item(i);
			if (node instanceof Element) {
				Gehoelz emp = new Gehoelz();

				NodeList childNodes = node.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node cNode = childNodes.item(j);

					// Identifying the child tag of employee encountered.
					if (cNode instanceof Element) {
						String content = cNode.getLastChild().getTextContent().trim();
						switch (cNode.getNodeName()) {
						case "art":
							emp.art = content;
							break;

						case "maxhoehe":
							emp.maxhoehe = content;
							break;
						case "preis":
							emp.preis = content;
							break;
						case "pflanzjahr":
							emp.pflanzjahr = content;
							break;
						}
					}
				}
				empList.add(emp);
			}

		}

		// Printing the Employee list populated.
		for (Gehoelz emp : empList) {
			System.out.println(emp);
		}
		System.out.println("runtime=" + (System.currentTimeMillis() - start) + " ms");

	}
}
