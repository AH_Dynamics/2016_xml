import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAX {

	public static void main(String[] args) throws Exception {
		

		SAXParserFactory parserFactor = SAXParserFactory.newInstance();

		SAXParser parser = parserFactor.newSAXParser();

		SAXHandler handler = new SAXHandler();

		parser.parse(ClassLoader.getSystemResourceAsStream("xml/daten.xml"),handler);

		// Printing the list of employees obtained from XML

		for (Gehoelz emp : handler.empList) {

			System.out.println(emp);
			
		}
		System.out.println("runtime=" + (System.currentTimeMillis()- handler.start) + " ms");

	}

}

/**
 * 
 * The Handler for SAX Events.
 * 
 */

class SAXHandler extends DefaultHandler {
	long start =System.currentTimeMillis();
	List<Gehoelz> empList = new ArrayList<>();

	Gehoelz emp = null;

	String content = null;

	@Override

	// Triggered when the start of tag is found.

	public void startElement(String uri, String localName,

			String qName, Attributes attributes)

					throws SAXException {

		switch (qName) {

		// Create a new Employee object when the start tag is found

		case "baum":

			emp = new Gehoelz();

			emp.art = attributes.getValue("art");

			break;

		}

	}

	@Override

	public void endElement(String uri, String localName,

			String qName) throws SAXException {

		switch (qName) {

		// Add the employee to list once end tag is found
		case "art":

			empList.add(emp);
			emp.art = content;

			break;

		

		// For all other end tags the employee has to be updated.

		case "pflanzjahr":

			emp.pflanzjahr = content;

			break;

		case "preis":

			emp.preis = content;

			break;

		case "maxhoehe":

			emp.maxhoehe = content;

			break;

		}

	}

	@Override

	public void characters(char[] ch, int start, int length)

			throws SAXException {

		content = String.copyValueOf(ch, start, length).trim();

	}

}
