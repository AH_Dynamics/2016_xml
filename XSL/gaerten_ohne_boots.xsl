<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
 xmlns:xsl=
    "http://www.w3.org/1999/XSL/Transform" 
 version="1.0"
 >
<xsl:output method="html"/>
 <xsl:template match="/">
      <xsl:text disable-output-escaping='yes'>&lt;!doctype html&gt;</xsl:text>
      <html lang="de">	  
	  <head>
	  <xsl:text disable-output-escaping='yes'>&lt;meta charset="UTF-8"&gt;</xsl:text>	  
	  </head>
	  <body>
	  <h1> Gaertnerei</h1>	  
         <xsl:apply-templates/>
      </body>
	  </html>
   </xsl:template>
   <xsl:template match="gehoelz">
   <h2><xsl:value-of select="@class"/></h2>
   <xsl:apply-templates/>
   </xsl:template>  
</xsl:stylesheet>