/*
 * Autor:				Achim H�ckler
 * Gruppe:				2913
 * Erstellungsdatum: 	11.2.2016
 */

package dtdcheck;

import java.io.IOException;

import javax.xml.parsers.*;
import org.w3c.dom.Document;
import org.xml.sax.*;

public class DOMdtdcheck {
	//Link auf datei
	private String link = "";
	
	public DOMdtdcheck(String myLink){
		//linkzuweisen
		this.link=myLink;
		
	}
	

	public void validate() {
		//Factory Objekt erstellen
		DocumentBuilderFactory meinefabrik = DocumentBuilderFactory.newInstance();
		//dokumentprüfung auf validirär einschalten
		meinefabrik.setValidating(true);
		//Dokumentenbauer initialisieren
		DocumentBuilder docbauer=null;
		try {
			//Dokumentbauer erschaffen
			docbauer = meinefabrik.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//ErrorHandler hinzufügen da SAXPaseException normalerweise ignoriert wird
		docbauer.setErrorHandler(new ErrorHandler() {
		    
		    public void error(SAXParseException exception) throws SAXException {		        
		        exception.printStackTrace();
		    }
		    
		    public void fatalError(SAXParseException exception) throws SAXException {
		        exception.printStackTrace();
		    }

		    
		    public void warning(SAXParseException exception) throws SAXException {
		        exception.printStackTrace();
		    }
		});
		//xml Datei parsen
		
			try {
				Document mydoc = docbauer.parse(this.link);
			} catch (SAXException e) {
				
				System.err.println("SAX Fehler");
			} catch (IOException e) {
				System.err.println("Fehler bei Datei einlesen");
			}
		

	}

}
