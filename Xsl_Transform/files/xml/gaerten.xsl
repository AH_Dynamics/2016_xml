<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
 xmlns:xsl=
    "http://www.w3.org/1999/XSL/Transform" 
 version="1.0"
 >
<xsl:output method="html"/>
 <xsl:template match="/">
      <xsl:text disable-output-escaping='yes'>&#60;!doctype html&#62;</xsl:text>
	  

	  
      <html lang="de">	  
		  <head>
		  <link href="bootstrap.min.css" type="text/css" rel="stylesheet" />		  
		  </head>
		  <body>
				<h1>Gaertnerei</h1>				
				<xsl:apply-templates/>									
		  </body>
	  </html>
</xsl:template>
   
<xsl:template match="gehoelz">   
					<div class="panel panel-default"> 
					<div class="panel-heading"> 
						<h3 class="panel-title"><xsl:value-of select="@class"/></h3> 
					</div> 					
					<table class="table-condensed"> 						
						<xsl:apply-templates/>						
					</table> 
					</div> 
</xsl:template>  

<xsl:template match="art">
		<tr><td>art</td><td><xsl:apply-templates/></td></tr>   
</xsl:template>

<xsl:template match="maxhoehe">
		<tr><td>maxhoehe</td><td><xsl:apply-templates/></td></tr>   
</xsl:template>

<xsl:template match="istgiftig">
		<tr><td>istgiftig</td><td><xsl:apply-templates/></td></tr>   
</xsl:template>

<xsl:template match="pflanzjahr">
		<tr><td>pflanzjahr</td><td><xsl:apply-templates/></td></tr>   
</xsl:template>

<xsl:template match="preis">
		<tr><td>preis</td><td><xsl:apply-templates/></td></tr>   
</xsl:template>

</xsl:stylesheet>