<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
 xmlns:xsl=
    "http://www.w3.org/1999/XSL/Transform" 
 version="1.0"
 >
<xsl:output method="html"/>
 <xsl:template match="/">
      <xsl:text disable-output-escaping='yes'>&#60;!doctype html&#62;</xsl:text>

	  
      <html lang="de">	  
		  <head>	   
		  </head>
		  <body>
				<h1>Gaertnerei</h1>
				<table>
				<xsl:apply-templates/>
				</table>
		  </body>
	  </html>
</xsl:template>
   
<xsl:template match="gehoelz">   
   <h2><xsl:value-of select="@class"/></h2>
   <tr>
   <ul>
		<xsl:apply-templates/>
   </ul>
   </tr>
</xsl:template>  
<xsl:template match="art|maxhoehe|istgiftig|pflanzjahr|preis">
   <li><xsl:apply-templates/></li>
</xsl:template>
</xsl:stylesheet>