/*
 * Autor:				Achim H�ckler
 * Gruppe:				2913
 * Erstellungsdatum: 	11.2.2016
 */

public class Start {
	public static void main(String[] args) {
		XSL_Transformer transi = new XSL_Transformer("files/xml/gaertnerei.xml","files/xml/gaerten.xsl");
		transi.transform();
	}

}
