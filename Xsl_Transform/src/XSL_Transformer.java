/*
 * Autor:				Achim H�ckler
 * Gruppe:				2913
 * Erstellungsdatum: 	11.2.2016
 */

import java.io.File;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;

public class XSL_Transformer {
	private String stylesheet = "";
    private String datei = "";
    //Links zu XML und XSL Datei im Konstruktor �bergeben
    XSL_Transformer(String xml, String xsl){
    	this.stylesheet=xsl;
    	this.datei=xml;
    }
    
    public void transform(){    
    //Fabrik erstellen
    TransformerFactory meinetransfabrik =TransformerFactory.newInstance();
    Transformer transformer = null;
    try {
    	//Transformer mit XSL Datei als Parameter erstellen
		transformer = meinetransfabrik.newTransformer(new StreamSource(new File(this.stylesheet)));
	} catch (TransformerConfigurationException e) {
		
		System.err.println("Fehler bei Transformerkonfiguration");
	}
   try {
	   // Umwandeln durch Transform-Aufruf. Datei die nach den Angaben in der XSL Datei Transformiert werden kann + Zieldatei f�r Ergebnis angeben
	transformer.transform(new StreamSource(new File(this.datei)), new StreamResult(new File("output.html")));
} catch (TransformerException e) {
	System.err.println("Transformer-Fehler");
}
  
  
    }

}
